#include <gsl/gsl_matrix.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_blas.h>
#include"givens.h"
int jacobi(gsl_matrix * A, gsl_matrix * V){
int sweeps=0, changed;
int n = A -> size1;
//gsl_matrix * VT = gsl_matrix_calloc(n,n);
//Jeg bruger jacobian functionen lavet til tidligere opgaver
do {
changed=0;
sweeps++;
for (int p = 0; p < n; p++) {
for (int q = p+1; q < n; q++) {
  double App = gsl_matrix_get(A,p,p), Aqq = gsl_matrix_get(A,q,q);
  double Apq = gsl_matrix_get(A,p,q);
  double phi = 0.5*atan2(2*Apq,Aqq-App);
  double c=cos(phi),s=sin(phi);
  double AApp = c*c*App-2*s*c*Apq+s*s*Aqq;
  double AAqq = s*s*App+2*s*c*Apq+c*c*Aqq;
  if (AApp!=App || AAqq!=Aqq) {
  	changed = 1;
  	givens_matrix JT = {.n=n,.p=p,.q=q,.theta=-phi};
  	givens_matrix J = {.n=n,.p=p,.q=q,.theta=phi};
	givens_matrix_left_mult(JT,A);
	givens_matrix_right_mult(A,J);
  givens_matrix_right_mult(V,J);



	}
}}}

while(changed!=0);
return sweeps;
}
