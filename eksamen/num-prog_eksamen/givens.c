#include<gsl/gsl_matrix.h>
#include<math.h>
#include"givens.h"

givens_matrix givens_matrix_transpose(givens_matrix J){
	givens_matrix JT = {.n=J.n,.p=J.p,.q=J.q,.theta=-J.theta};
	return JT;
}
//Her ganges J^T * A
void givens_matrix_left_mult(givens_matrix J,gsl_matrix* A){
	double c=cos(J.theta), s=sin(J.theta);
	int p=J.p,q=J.q;
	for(int i=0;i<J.n;i++){
		double Api= c*gsl_matrix_get(A,p,i)+s*gsl_matrix_get(A,q,i);
		double Aqi=-s*gsl_matrix_get(A,p,i)+c*gsl_matrix_get(A,q,i);
		gsl_matrix_set(A,p,i,Api);
		gsl_matrix_set(A,q,i,Aqi);
	}
}
//Her ganges (J^T * A)*J
void givens_matrix_right_mult(gsl_matrix* A,givens_matrix J){
	double c=cos(J.theta), s=sin(J.theta);
	int p=J.p,q=J.q;
	for(int i=0;i<J.n;i++){
		double Aip=c*gsl_matrix_get(A,i,p)-s*gsl_matrix_get(A,i,q);
		double Aiq=s*gsl_matrix_get(A,i,p)+c*gsl_matrix_get(A,i,q);
		gsl_matrix_set(A,i,p,Aip);
		gsl_matrix_set(A,i,q,Aiq);

	}
}
