//Generalized eigenvalue problem
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include<stdio.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

int jacobi(gsl_matrix *, gsl_matrix *);

void matrix_print(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++)printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}}

void vector_print(gsl_vector *v){
	for(int i=0;i<v->size;i++) printf(FMT,gsl_vector_get(v,i));
	printf("\n");
	}

int main(){
//valg af matrix dimention
int n = 6;
//A laves som reel symmetrisk matricw med tilfældige tal og N som reel symetrisk positivt defineret matrix
gsl_matrix *N = gsl_matrix_alloc(n,n);
gsl_matrix *I = gsl_matrix_alloc(n,n);
gsl_matrix_set_identity(I);
gsl_matrix *A = gsl_matrix_alloc(n,n);
for(int i=0;i<n;i++) for(int j=i;j<n;j++) {
	double x = RND;
	gsl_matrix_set(A,i,j,x);
	gsl_matrix_set(A,j,i,x);
	double test = RND;
	gsl_matrix_set(N,i,j,test);
	gsl_matrix_set(N,j,i,test);
}
//N=N+dim(I)*I, hvor I er identitets matricen. DEtte gør vi altid får positive egenværdier
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,n,I,I,1.0,N);
gsl_matrix * N2= gsl_matrix_alloc(n,n);
gsl_matrix_memcpy(N2,N);
//V matricer til brug i jacobian funktionen
gsl_matrix * V = gsl_matrix_calloc(n,n);
gsl_matrix_set_identity(V);
gsl_matrix * V2 = gsl_matrix_calloc(n,n);
gsl_matrix_set_identity(V2);

gsl_matrix * sqrt_D= gsl_matrix_alloc(n,n); gsl_matrix * sqrt_D_invers= gsl_matrix_alloc(n,n);
gsl_matrix * DV= gsl_matrix_alloc(n,n); gsl_matrix * DV_y= gsl_matrix_alloc(n,n);
gsl_matrix * DVA= gsl_matrix_alloc(n,n); gsl_matrix * DVAV= gsl_matrix_alloc(n,n);
gsl_matrix * B= gsl_matrix_alloc(n,n); gsl_matrix * x= gsl_matrix_calloc(n,n);
gsl_matrix * DV2= gsl_matrix_alloc(n,n); gsl_matrix * V_invers= gsl_matrix_calloc(n,n);
gsl_vector * resultat1= gsl_vector_alloc(n); gsl_vector * resultat2= gsl_vector_alloc(n);
gsl_vector * x1 =gsl_vector_alloc(n);
//Udskriv A og N
printf("A:\n");
matrix_print(A);
printf("N:\n");
matrix_print(N);

//Bestemmelse af egenvektore og værdier for N
jacobi(N,V);
printf("D=V^T*A*V, eigenvalues på diagonalen\n");
matrix_print(N);
printf("V=1*J*J*J osv., eigenvectore\n");
matrix_print(V);
//N=D
//Find B i ligningen B y=lambda y
// B sqrt(D⁻1) V^T A V sqrt(D)
double indgang;
//Udregning af kvaderatroden og invers kvaderatrod af D
for (size_t i = 0; i < n; i++){
  indgang = gsl_matrix_get(N,i,i);
  gsl_matrix_set(sqrt_D,i,i,pow(indgang,0.5));
  gsl_matrix_set(sqrt_D_invers,i,i,pow(indgang,-0.5));
}
printf("sqrt(D):\n");
matrix_print(sqrt_D);
printf("sqrt(D^⁻1):\n");
matrix_print(sqrt_D_invers);

gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,sqrt_D_invers,V,0.0,DV);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,DV,A,0.0,DVA);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,DVA,V,0.0,DVAV);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,DVAV,sqrt_D_invers,0.0,B);
printf("B:\n");
matrix_print(B);
//Bestemmelse af egenvektore og værdier for N
// find y=sqrt(D) V^T x
jacobi(B,V2);
printf("egenværdier for B\n");
matrix_print(B);
printf("Egenvektore for B\n");
matrix_print(V2);

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,V,sqrt_D_invers,0.0,DV2);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,DV2,V2,0.0,x);
printf("x, generaliserede egenværdier for A\n");
matrix_print(x);


//Her undersøges om Ax=lambda*N*x er opfyldt
printf("Test om A*x=lambda*N*x for alle egenværdier og vektore\n");
gsl_matrix_get_col(x1,x,0);
for (size_t i = 0; i < n; i++) {
gsl_matrix_get_col(x1,x,i);
gsl_blas_dgemv (CblasNoTrans,1.0,A,x1,0.0,resultat1);
printf("resultat1=A*x\n");
vector_print(resultat1);
double lambda = gsl_matrix_get(B,i,i);
gsl_matrix_get_col(x1,x,i);
gsl_blas_dgemv (CblasNoTrans,lambda,N2,x1,0.0,resultat2);
printf("resultat2=lambda*N*x\n");
vector_print(resultat2);
printf("resultat1=resultat2\n");
}



gsl_matrix_free(A); gsl_matrix_free(N); gsl_matrix_free(DV); gsl_matrix_free(DVA); gsl_matrix_free(V);
gsl_matrix_free(DVAV); gsl_matrix_free(B); gsl_matrix_free(sqrt_D);  gsl_matrix_free(sqrt_D_invers);
gsl_matrix_free(DV_y); gsl_matrix_free(V2); gsl_matrix_free(x);  gsl_matrix_free(DV2); gsl_matrix_free(V_invers);
gsl_vector_free(resultat1); gsl_vector_free(resultat2); gsl_matrix_free(I);
gsl_vector_free(x1);


return 0;
}
